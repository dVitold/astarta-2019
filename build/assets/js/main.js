'use strict';

var qs = function qs(sel) {
	return document.querySelector(sel);
};
var qsa = function qsa(sel) {
	return document.querySelectorAll(sel);
};
var APP = {
	name: 'HTML Starter'
};

(function () {
	// global object. DONT change it
	var AS = {};

	// global variables
	AS.ESC_CODE = 27;
	AS.body = document.querySelector('body');

	// brakepoints
	AS.fromDesktop = window.matchMedia("(min-width: 1025px)");
	AS.lessTablet = window.matchMedia("(max-width: 1024px) and (min-width:768px)");
	AS.atMobile = window.matchMedia("(max-width: 767px)");

	// utils
	AS.accordion = function () {
		var $accordion = $('.js-accordion');
		var $accordionButtons = $accordion.find('.js-accordion-btn');
		var $accordionContents = $accordion.find('.js-accordion-content');
		var $accordionItems = $accordion.find('.js-accordion-item');

		var globalAccordionMobileClose = function globalAccordionMobileClose() {
			var $accordionMobile = $('.js-accordion-mobile');
			var $content = $accordionMobile.find('.js-accordion-content');

			$accordionMobile.removeClass('active');
			$content.hide();
		};

		var globalAccordionMobileOpen = function globalAccordionMobileOpen() {
			var $accordionMobile = $('.js-accordion-mobile');
			var $content = $accordionMobile.find('.js-accordion-content');

			$accordionMobile.addClass('active');
			$content.show();
		};

		var globalAccordionClose = function globalAccordionClose() {
			$accordionContents.each(function () {
				var $parent = $(this).closest('.js-accordion');
				var isMobileAccordion = $parent.hasClass('js-accordion-mobile');
				var $item = $parent.find('.js-accordion-item');

				if (isMobileAccordion) {} else {
					$item.removeClass('active');
					$(this).slideUp('400');
				}
			});
		};

		var globalAccordionOpen = function globalAccordionOpen() {
			var isAccordionList = $accordionItems.closest('.js-accordion--list').length === 0;

			if (isAccordionList) {
				$accordionItems.addClass('active');
				$accordionContents.slideDown('400');
			}
		};

		$accordionButtons.on('click', function () {
			var $parentItem = $(this).closest('.js-accordion-item');
			var $parentContent = $parentItem.find('.js-accordion-content');
			var isActive = $parentItem.hasClass('active');

			var accordionOpen = function accordionOpen() {
				$parentItem.removeClass('active');
				$parentContent.slideUp('400');
			};

			var accordionClose = function accordionClose() {
				$parentItem.addClass('active');
				$parentContent.slideDown('400');
			};

			if (isActive) {
				accordionOpen();
			} else {
				accordionClose();
			}
		});

		var width = $(window).width();

		$(window).on('resize', function () {
			var isVertical = $(window).width() == width;

			if (isVertical) {} else {
				if (AS.lessTablet.matches || AS.atMobile.matches) {
					globalAccordionClose();
				} else {
					globalAccordionOpen();
				}

				if (AS.atMobile.matches) {
					globalAccordionMobileClose();
				} else {
					globalAccordionMobileOpen();
				}
			}

			width = $(window).width();
		});

		if (AS.lessTablet.matches || AS.atMobile.matches) {
			globalAccordionClose();
		} else {
			globalAccordionOpen();
		}

		if (AS.atMobile.matches) {
			globalAccordionMobileClose();
		} else {
			globalAccordionMobileOpen();
		}
	};

	AS.select = function () {
		var $selects = $('.js-select');

		$selects.select2({
			dropdownAutoWidth: true,
			width: 'auto'
		});
	};

	AS.isFormElemFilled = function () {
		var $elems = $('.js-form-elem');

		$elems.each(function () {
			$(this).on('keyup', function () {
				var $parent = $(this).closest('.js-form-item');
				var valuelength = $(this).val().length;
				var isElemFilled = valuelength >= 1;

				if (isElemFilled) {
					$parent.addClass('active');
				} else {
					$parent.removeClass('active');
				}
			});
		});
	};

	AS.phoneMask = function () {
		var $phoneElems = $('.js-phone-mask');

		$phoneElems.each(function () {
			$(this).mask('+0 (000) 000-00-00');
		});
	};

	AS.toggleClass = function () {
		var $elems = $('.js-toggle-class');

		$elems.each(function () {
			$(this).on('click', function () {
				var isActive = $(this).hasClass('active');

				if (isActive) {
					$(this).removeClass('active');
				} else {
					$(this).addClass('active');
				}
			});
		});
	};

	AS.elemsOnClickCloseOutside = function (elem, closeFunction) {
		var isVisible = function isVisible(elem) {
			return !!elem && !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
		};

		var outsideClickListener = function outsideClickListener(event) {
			if (!elem.contains(event.target) && isVisible(elem)) {
				closeFunction(elem);
				document.removeEventListener('click', outsideClickListener);
			}
		};

		document.addEventListener('click', outsideClickListener);
	};

	AS.togglePassword = function () {
		var elems = document.querySelectorAll('.js-toggle-password');

		elems.forEach(function (elem) {
			elem.addEventListener('click', function () {
				var parent = this.closest('.js-toggle-password-parent');
				var input = parent.querySelector('.js-toggle-password-input');
				var isActive = this.classList.contains('active');

				if (isActive) {
					input.type = "text";
				} else {
					input.type = "password";
				}
			});
		});
	};

	AS.bodyOverflow = function () {
		var elems = document.querySelectorAll('.js-hamburger');

		elems.forEach(function (elem) {
			elem.addEventListener('click', function () {
				var parent = this.closest('.js-animation-parent');
				var isActive = parent.classList.contains('active');

				if (isActive) {
					AS.body.classList.remove('overflow');
				} else {
					AS.body.classList.add('overflow');
				}
			});
		});
	};

	AS.adaptiveImg = function () {
		var $elems = $('.js-adaptive-bg');

		var changeImg = function changeImg() {
			var isDesktop = AS.fromDesktop.matches;
			var isTablet = AS.lessTablet.matches;
			var isMobile = AS.atMobile.matches;

			var changeSrc = function changeSrc($this, breakpoint) {
				var imgSrc = $this.data('bg-' + breakpoint + '');

				if (imgSrc === '') {
					imgSrc = $this.data('bg-desktop');
				}

				$this.css('background-image', 'url(' + imgSrc + ')');
			};

			$elems.each(function (i, elem) {
				if (isDesktop) {
					changeSrc($(this), 'desktop');
				} else if (isTablet) {
					changeSrc($(this), 'tablet');
				} else if (isMobile) {
					changeSrc($(this), 'mobile');
				}
			});
		};

		$(window).on('resize', function () {
			changeImg();
		});

		changeImg();
	};

	AS.wowJs = function () {
		var wow = new WOW({
			mobile: false
		});

		wow.init();
	};

	AS.tabs = function () {
		$(document).on('click', '[data-tabclass]', function onClick(e) {
			var $this = $(this);
			var content = $this.data('tabclass');
			var number = $this.data('tabnumber');
			var isHref = $(this).attr('href');

			if (isHref) {
				e.preventDefault();
			}

			$('[data-tabclass="' + content + '"]').each(function each() {
				var $element = $(this);

				if ($element.data('tabnumber') === number) {
					$element.addClass('active').siblings().removeClass('active');
				}
			});

			$('.' + content + ' > [data-tabnumber="' + number + '"]').addClass('active').siblings().removeClass('active');
		});
	};

	AS.tags = function () {
		var tags = document.querySelectorAll('.js-tags-input');

		tags.forEach(function (elem) {
			elem.addEventListener('click', function () {
				var parent = elem.closest('.js-tags-li');
				var elemList = parent.closest('.js-tags-list');
				var elemSiblings = elemList.querySelectorAll('.js-tags-li');
				var isChecked = elem.checked;

				var checked = function checked() {
					elemSiblings.forEach(function (elem) {
						elem.classList.remove('active');
					});
					parent.classList.add('active');
				};

				if (isChecked) {
					checked();
				}
			});
		});
	};

	// promocode events
	AS.promocodeEvents = function () {
		var $elems = $('.js-promocode');

		$elems.each(function () {
			var $parentElem = $(this);
			var $input = $parentElem.find('.promocode__input');
			var $btnReset = $parentElem.find('.js-promocode-reset');

			var isFilled = function isFilled() {
				$btnReset.addClass('active');
			};

			var isNotFilled = function isNotFilled() {
				$btnReset.removeClass('active');
			};

			var inputClear = function inputClear() {
				$input.val('');
			};

			inputClear();

			$input.on('keyup', function () {
				var value = $(this).val();
				var isValue = value.length >= 1;

				if (isValue) {
					isFilled();
				} else {
					isNotFilled();
				}
			});

			$btnReset.on('click', function () {
				inputClear();
				isNotFilled();
			});
		});
	};

	// specific
	AS.checkboxReset = function () {
		var $btns = $('.js-filter-reset');

		$btns.each(function () {
			$(this).on('click', function () {
				var $parentItem = $(this).closest('.js-checkbox-reset-item');
				var $inputs = $parentItem.find('.js-checkbox-input');

				$inputs.prop('checked', false);
			});
		});
	};

	AS.headerScrolled = function () {
		var elems = document.querySelectorAll('.js-page-header');

		window.addEventListener('scroll', function (e) {
			var pageY = e.pageY;
			var actionPoint = pageY >= 80;

			elems.forEach(function (elem) {
				if (actionPoint) {
					elem.classList.add('scrolled');
				} else {
					elem.classList.remove('scrolled');
				}
			});
		});
	};

	AS.scrollUp = function () {
		var $elems = $('.js-scroll-up');

		$elems.on('click', function () {
			$('html, body').animate({
				scrollTop: 0
			}, 500);
		});
	};

	AS.scrollTo = function () {
		var $elems = $('.js-scroll-elem');

		$elems.on('click', function (e) {
			e.preventDefault;
			var scrollTarget = $(this).data('target');
			var $target = $('.' + scrollTarget);
			var targetOffset = $target.offset().top - 100;

			$('html, body').animate({
				scrollTop: targetOffset
			}, 500);
		});
	};

	AS.searchEvents = function () {
		var $elems = $('.js-search-form');

		$elems.each(function () {
			var $parentElem = $(this);
			var $input = $parentElem.find('.js-search-input');
			var $btnReset = $parentElem.find('.js-search-reset');

			var isFilled = function isFilled() {
				$parentElem.addClass('filled');
			};

			var isNotFilled = function isNotFilled() {
				$parentElem.removeClass('filled');
			};

			var inputClear = function inputClear() {
				$input.val('');
			};

			inputClear();

			$input.on('keyup', function () {
				var value = $(this).val();
				var isValue = value.length > 0;

				if (isValue) {
					isFilled();
				} else {
					isNotFilled();
				}
			});

			$btnReset.on('click', function () {
				inputClear();
				isNotFilled();
			});
		});
	};

	AS.linkTo = function () {
		var elems = document.querySelectorAll('.js-link');

		elems.forEach(function (elem) {
			elem.addEventListener("click", function () {
				var url = this.getAttribute('data-href');

				window.location.href = url;
			});
		});
	};

	AS.rangeSlider = function () {
		var $sliders = $('.js-range-slider');
		var $resetBtns = $('.js-aside-slider-reset');
		var $valueElems = $('.js-slider-range-values');
		var $minValueElems = $('.js-slider-range-value-min');
		var $maxValueElems = $('.js-slider-range-value-max');

		// on slide event inside
		var options = {
			range: true,
			min: 0,
			max: 7000,
			values: [0, 7000],
			slide: function slide(event, ui) {
				var $parent = $(this).closest('.js-slider-range');
				var $minValue = $parent.find('.js-slider-range-value-min');
				var $maxValue = $parent.find('.js-slider-range-value-max');
				var $filter = $parent.find('.js-slider-range-filter');
				var positionTop = $parent.position().top;

				$minValue.val(ui.values[0]);
				$maxValue.val(ui.values[1]);
				$filter.addClass('active');
				AS.changeFilterPosition(positionTop);
			}
		};

		var hideDefaultInputArrows = function hideDefaultInputArrows($valueElems) {
			$valueElems.on('focus', function (e) {
				$(this).on('wheel', function (e) {
					e.preventDefault();
				});
			});
		};

		var onPriceChange = function onPriceChange() {
			$valueElems.on('change', function () {
				var $parent = $(this).closest('.js-slider-range');
				var positionTop = $parent.position().top;
				console.log(123);

				AS.changeFilterPosition(positionTop);
			});
		};

		var changeSliderPosition = function changeSliderPosition($minValueElems, $maxValueElems) {
			var changePositionMin = function changePositionMin($this) {
				var $parent = $this.closest('.js-aside-slider-reset-parent');
				var $slider = $parent.find('.js-range-slider');
				var value = $this.val();

				$slider.slider('values', 0, value);
			};

			var changePositionMax = function changePositionMax($this) {
				var $parent = $this.closest('.js-aside-slider-reset-parent');
				var $slider = $parent.find('.js-range-slider');
				var value = $this.val();

				$slider.slider('values', 1, value);
			};

			$minValueElems.each(function () {
				$(this).on('change', function () {
					changePositionMin($(this));
				});
			});

			$maxValueElems.each(function () {
				$(this).on('change', function () {
					changePositionMax($(this));
				});
			});
		};

		var resetSlider = function resetSlider($resetBtns) {
			$resetBtns.each(function () {
				var $this = $(this);

				$this.on('click', function () {
					var $parent = $this.closest('.js-aside-slider-reset-parent');
					var $slider = $parent.find('.js-range-slider');
					var $minValueElem = $parent.find('.js-slider-range-value-min');
					var $maxValueElem = $parent.find('.js-slider-range-value-max');
					var $minValueVal = $minValueElem.attr('data-value');
					var $maxValueVal = $maxValueElem.attr('data-value');

					$slider.slider('values', 0, $minValueVal);
					$slider.slider('values', 1, $maxValueVal);

					$minValueElem.val($minValueVal);
					$maxValueElem.val($maxValueVal);
				});
			});
		};

		var initialization = function initialization() {
			onPriceChange();
			hideDefaultInputArrows($valueElems);
			$sliders.slider(options);
			changeSliderPosition($minValueElems, $maxValueElems);
			resetSlider($resetBtns);
			$sliders.draggable();
		};

		initialization();
	};

	AS.popupEmail = function () {
		var $btnsOpen = $('.js-popup-email-open');
		var $btnsClose = $('.js-popup-email-close');
		var $popups = $('.js-popup-email');
		var $body = $('body');

		var popupShow = function popupShow($popup) {
			$popup.addClass('active');
			$body.addClass('overflow');
		};

		var popupHide = function popupHide($popup) {
			$popup.removeClass('active');
			$body.removeClass('overflow');
		};

		var globalPopupHide = function globalPopupHide() {
			$popups.removeClass('active');
			$body.removeClass('overflow');
		};

		$popups.each(function () {
			var $container = $(this).find('.js-popup-email-container');

			$container.on('click', function (event) {
				event.stopPropagation();
			});
		});

		$btnsOpen.each(function () {
			$(this).on('click', function (event) {
				var popupSelector = $(this).data('popup-selector');
				var $popup = $('.' + popupSelector);
				var isActive = $popup.hasClass('active');

				event.stopPropagation();

				if (isActive) {
					popupHide($popup);
				} else {
					popupShow($popup);
				}
			});
		});

		$btnsClose.each(function () {
			$(this).on('click', function () {
				var $popup = $(this).closest('.js-popup-email');

				popupHide($popup);
			});
		});

		$(document).on('keydown.js-popup-email', function onKeyDown(evt) {
			if (evt.keyCode === AS.ESC_CODE) {
				globalPopupHide();
			}
		});
	};

	AS.popups = function () {
		var popups = document.querySelectorAll('.js-popup');
		var body = document.querySelector('body');
		var btnsOpen = document.querySelectorAll('.js-popup-btn-open');

		var popupOpen = function popupOpen(btnOpen) {
			var popupData = btnOpen.getAttribute('data-popup-selector');
			var popup = document.querySelector('.' + popupData);

			body.classList.add('overflow');
			popup.classList.add('active');
		};

		var popupClose = function popupClose(btnClose) {
			var popupParent = btnClose.closest('.js-popup');

			body.classList.remove('overflow');
			popupParent.classList.remove('active');
		};

		var popupGlobalClose = function popupGlobalClose() {
			body.classList.add('overflow');
			popups.forEach(function (popup) {
				popup.classList.remove('active');
			});
		};

		popups.forEach(function (popup) {
			var btnsClose = popup.querySelectorAll('.js-popup-btn-close');

			btnsClose.forEach(function (btnClose) {
				btnClose.addEventListener('click', function () {
					popupClose(this);
				});
			});
		});

		btnsOpen.forEach(function (btnOpen) {
			btnOpen.addEventListener('click', function () {
				popupOpen(this);
			});
		});

		$(document).on('keydown.js-popup', function onKeyDown(evt) {
			if (evt.keyCode === AS.ESC_CODE) {
				popupGlobalClose();
			}
		});
	};

	AS.filterMove = function () {
		var checkboxes = document.querySelectorAll('.js-checkbox-filter');
		AS.filter = document.querySelector('.js-slider-range-filter');

		AS.changeFilterPosition = function (positionTop) {
			if (AS.lessTablet.matches) {
				return;
			}

			if (AS.filter) {
				AS.filter.classList.add('active');
				AS.filter.style.top = positionTop + 'px';
			}
		};

		var hideFilter = function hideFilter() {
			if (AS.filter) {
				AS.filter.classList.remove('active');
			}
		};

		checkboxes.forEach(function (checkbox) {
			checkbox.addEventListener('change', function () {
				var isChecked = this.checked;
				var positionTop = this.closest('.checkbox').offsetTop;

				if (isChecked) {
					AS.changeFilterPosition(positionTop);
				}
			});
		});

		window.addEventListener('resize', function () {
			hideFilter();
		});
	};

	AS.popupMsg = function (text) {
		var popup = document.querySelector('.js-popup-any-message');
		var textSelector = popup.querySelector('.js-popup-message');
		var body = document.querySelector('body');

		body.classList.add('overflow');
		popup.classList.add('active');
		textSelector.textContent = text;
	};

	// animation
	AS.childAnimation = function () {
		var elemsOnClick = document.querySelectorAll('.js-animation-parent--click');
		var elemsOnHover = document.querySelectorAll('.js-animation-parent--hover');
		var elemsOnClickTarget = document.querySelectorAll('.js-animation-target');
		var btnsClose = document.querySelectorAll('.js-animation-close');

		var animationFunctionVanilla = function animationFunctionVanilla(elem) {
			var parent = elem.closest('.js-animation-parent');
			var isAnimationTarget = elem.classList.contains('js-animation-target');

			if (parent === null) {
				parent = elem;
			}

			var isActive = parent.classList.contains('active');
			var animationStart = elem.getAttribute('data-animation-start');
			var animationEnd = elem.getAttribute('data-animation-end');
			var child = parent.querySelector('.js-animation-child');

			if (isAnimationTarget) {
				child = null;
			}

			if (child === null) {
				var animationSelector = elem.getAttribute('data-animation-selector');
				child = document.querySelector('.' + animationSelector);
			}

			var startAnimation = function startAnimation(elem) {
				elem.classList.add('active');
				child.classList.remove(animationEnd);
				child.classList.add(animationStart);
			};

			var endAnimation = function endAnimation(elem) {
				elem.classList.remove('active');
				child.classList.add(animationEnd);
				child.classList.remove(animationStart);
			};

			if (!isActive) {
				startAnimation(parent);
			} else {
				endAnimation(parent);
			}

			AS.elemsOnClickCloseOutside(parent, endAnimation);
		};

		var animationClose = function animationClose(elem) {
			var parent = elem.closest('.js-animation-parent');
			var child = parent.querySelector('.js-animation-child');
			var animationBtn = parent.querySelector('.js-animation-parent--click');
			var animationStart = animationBtn.getAttribute('data-animation-start');
			var animationEnd = animationBtn.getAttribute('data-animation-end');

			parent.classList.remove('active');
			child.classList.add(animationEnd);
			child.classList.remove(animationStart);
		};

		elemsOnClick.forEach(function (elem) {
			elem.addEventListener('click', function () {
				animationFunctionVanilla(this);
			});
		});

		elemsOnClickTarget.forEach(function (elem) {
			elem.addEventListener('click', function () {
				animationFunctionVanilla(this);
			});
		});

		btnsClose.forEach(function (elem) {
			elem.addEventListener('click', function () {
				animationClose(elem);
			});
		});

		elemsOnHover.forEach(function (elem) {
			elem.addEventListener('mouseenter', function (e) {
				var elem = e.target;

				animationFunctionVanilla(elem);
			});

			elem.addEventListener('mouseleave', function (e) {
				var elem = e.target;

				animationFunctionVanilla(elem);
			});
		});
	};

	// sliders
	AS.singleSLider = function () {
		var $slider = $('.js-single-slider');

		$slider.slick({
			dots: true,
			slidesToShow: 1,
			arrows: false,
			infinite: true,
			appendDots: '.js-single-slider-dots'
		});
	};

	AS.brandSLider = function () {
		var $slider = $('.js-brand-slider');
		var $arrPrev = $('.js-brand-arr-prev');
		var $arrNext = $('.js-brand-arr-next');

		$slider.slick({
			dots: false,
			slidesToShow: 3,
			arrows: false,
			infinite: false,
			responsive: [{
				breakpoint: 1023,
				settings: {
					slidesToShow: 2
				}
			}]
		});

		$arrPrev.on('click', function () {
			$slider.slick('slickPrev');
		});

		$arrNext.on('click', function () {
			$slider.slick('slickNext');
		});
	};

	AS.catalogShortSlider = function () {
		var $slider = $('.js-catalog-short-slider');
		var $window = $(window);

		function throttle(fn, frequency) {
			'use strict';

			frequency = frequency || 100;
			var timeout = false;

			return function () {
				if (timeout) {
					return;
				}

				timeout = setTimeout(function () {
					timeout = false;
				}, frequency);

				fn.apply(this, arguments);
			};
		}

		var slickOptions = {
			arrows: false,
			dots: true,
			slidesToShow: 4,
			infinite: true,
			responsive: [{
				breakpoint: 767,
				settings: {
					slidesToShow: 2
				}
			}]
		};

		$slider.slick(slickOptions).each(function () {
			function equalizeHeights() {
				var $this = $(this);
				var $track = $this.find('.slick-track');
				var $slides = $track.children();
				var $slidesDiv = $slides.children();
				var $slidesItem = $slidesDiv.children();
				var $slidesContent = $slidesItem.children();

				$slides.css('min-height', '');
				$slidesDiv.css('min-height', '');
				$slidesItem.css('min-height', '');
				$slidesContent.css('min-height', '');

				var minSlideHeight = $track.height();

				$slides.css('min-height', minSlideHeight);
				$slidesDiv.css('min-height', minSlideHeight);
				$slidesItem.css('min-height', minSlideHeight);
				$slidesContent.css('min-height', minSlideHeight);
			}

			var equalizeSlideHeights = throttle(equalizeHeights.bind(this), 250, true);
			$window.on('DOMContentLoaded load resize', equalizeSlideHeights);
		});
	};

	AS.catalogElemSlider = function () {
		var $slider = $('.js-catalog-elem-slider');

		var slickOptions = {
			arrows: false,
			dots: true,
			slidesToShow: 1,
			infinite: true,
			appendDots: '.catalog-elem__dots'
		};

		$slider.slick(slickOptions);
	};

	// utils
	AS.toggleClass();
	AS.accordion();
	AS.select();
	AS.isFormElemFilled();
	AS.phoneMask();
	AS.togglePassword();
	AS.bodyOverflow();
	AS.adaptiveImg();
	AS.wowJs();
	AS.tabs();
	AS.promocodeEvents();
	AS.tags();

	// specific
	AS.checkboxReset();
	AS.headerScrolled();
	AS.scrollUp();
	AS.searchEvents();
	AS.linkTo();
	AS.rangeSlider();
	AS.popupEmail();
	AS.popups();
	AS.filterMove();
	AS.scrollTo();

	// animation
	AS.childAnimation();

	// sliders
	AS.singleSLider();
	AS.brandSLider();
	AS.catalogShortSlider();
	AS.catalogElemSlider();
})();
//# sourceMappingURL=main.js.map
