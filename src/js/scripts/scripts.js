(function () {
	// global object. DONT change it
	const AS = {};

	// global variables
	AS.ESC_CODE = 27;
	AS.body = document.querySelector('body');

	// brakepoints
	AS.fromDesktop = window.matchMedia("(min-width: 1025px)");
	AS.lessTablet = window.matchMedia("(max-width: 1024px) and (min-width:768px)");
	AS.atMobile = window.matchMedia("(max-width: 767px)");

	// utils
	AS.accordion = function () {
		const $accordion = $('.js-accordion');
		const $accordionButtons = $accordion.find('.js-accordion-btn');
		const $accordionContents = $accordion.find('.js-accordion-content');
		const $accordionItems = $accordion.find('.js-accordion-item');

		const globalAccordionMobileClose = function () {
			const $accordionMobile = $('.js-accordion-mobile');
			const $content = $accordionMobile.find('.js-accordion-content');

			$accordionMobile.removeClass('active');
			$content.hide();
		};

		const globalAccordionMobileOpen = function () {
			const $accordionMobile = $('.js-accordion-mobile');
			const $content = $accordionMobile.find('.js-accordion-content');

			$accordionMobile.addClass('active');
			$content.show();
		};

		const globalAccordionClose = function () {
			$accordionContents.each(function () {
				const $parent = $(this).closest('.js-accordion');
				const isMobileAccordion = $parent.hasClass('js-accordion-mobile');
				const $item = $parent.find('.js-accordion-item');

				if(isMobileAccordion) {

				} else {
					$item.removeClass('active');
					$(this).slideUp('400');
				}
			});
		};

		const globalAccordionOpen = function () {
			const isAccordionList = $accordionItems.closest('.js-accordion--list').length === 0;

			if(isAccordionList) {
				$accordionItems.addClass('active');
				$accordionContents.slideDown('400');
			}
		};

		$accordionButtons.on('click', function () {
			const $parentItem = $(this).closest('.js-accordion-item');
			const $parentContent = $parentItem.find('.js-accordion-content');
			const isActive = $parentItem.hasClass('active');

			const accordionOpen = function () {
				$parentItem.removeClass('active');
				$parentContent.slideUp('400');
			};

			const accordionClose = function () {
				$parentItem.addClass('active');
				$parentContent.slideDown('400');
			};

			if(isActive) {
				accordionOpen();
			} else {
				accordionClose();
			}
		});

		let width = $(window).width();

		$(window).on('resize', function () {
			const isVertical = $(window).width() == width;

			if(isVertical) {

			} else {
				if(AS.lessTablet.matches || AS.atMobile.matches) {
					globalAccordionClose();
				} else {
					globalAccordionOpen();
				}

				if(AS.atMobile.matches) {
					globalAccordionMobileClose();
				} else {
					globalAccordionMobileOpen();
				}
			}

			width = $(window).width();
		});

		if(AS.lessTablet.matches || AS.atMobile.matches) {
			globalAccordionClose();
		} else {
			globalAccordionOpen();
		}

		if(AS.atMobile.matches) {
			globalAccordionMobileClose();
		} else {
			globalAccordionMobileOpen();
		}
	};

	AS.select = function () {
		const $selects = $('.js-select');

		$selects.select2({
			dropdownAutoWidth: true,
			width: 'auto'
		});
	};

	AS.isFormElemFilled = function () {
		const $elems = $('.js-form-elem');

		$elems.each(function () {
			$(this).on('keyup', function () {
				const $parent = $(this).closest('.js-form-item');
				const valuelength = $(this).val().length;
				const isElemFilled = valuelength >= 1;

				if(isElemFilled) {
					$parent.addClass('active');
				} else {
					$parent.removeClass('active');
				}
			});
		});
	};

	AS.phoneMask = function () {
		const $phoneElems = $('.js-phone-mask');

		$phoneElems.each(function () {
			$(this).mask('+0 (000) 000-00-00');
		});
	};

	AS.toggleClass = function () {
		const $elems = $('.js-toggle-class');

		$elems.each(function () {
			$(this).on('click', function () {
				const isActive = $(this).hasClass('active');

				if(isActive) {
					$(this).removeClass('active');
				} else {
					$(this).addClass('active');
				}
			});
		});
	};

	AS.elemsOnClickCloseOutside = function (elem, closeFunction) {
		const isVisible = function (elem) {
			return !!elem && !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
		}

		const outsideClickListener = function (event) {
			if(!elem.contains(event.target) && isVisible(elem)) {
				closeFunction(elem);
				document.removeEventListener('click', outsideClickListener);
			}
		}

		document.addEventListener('click', outsideClickListener)
	};

	AS.togglePassword = function () {
		const elems = document.querySelectorAll('.js-toggle-password');

		elems.forEach(function (elem) {
			elem.addEventListener('click', function () {
				const parent = this.closest('.js-toggle-password-parent');
				const input = parent.querySelector('.js-toggle-password-input');
				const isActive = this.classList.contains('active');

				if(isActive) {
					input.type = "text";
				} else {
					input.type = "password";
				}
			});
		});
	};

	AS.bodyOverflow = function () {
		const elems = document.querySelectorAll('.js-hamburger');

		elems.forEach(function (elem) {
			elem.addEventListener('click', function () {
				const parent = this.closest('.js-animation-parent');
				const isActive = parent.classList.contains('active');

				if(isActive) {
					AS.body.classList.remove('overflow');
				} else {
					AS.body.classList.add('overflow');
				}
			});
		});
	};

	AS.adaptiveImg = function () {
		const $elems = $('.js-adaptive-bg');

		const changeImg = function () {
			const isDesktop = AS.fromDesktop.matches;
			const isTablet = AS.lessTablet.matches;
			const isMobile = AS.atMobile.matches;

			const changeSrc = function ($this, breakpoint) {
				let imgSrc = $this.data('bg-' + breakpoint + '');

				if(imgSrc === '') {
					imgSrc = $this.data('bg-desktop');
				}

				$this.css('background-image', 'url(' + imgSrc + ')');
			};

			$elems.each(function (i, elem) {
				if(isDesktop) {
					changeSrc($(this), 'desktop');
				} else if(isTablet) {
					changeSrc($(this), 'tablet');
				} else if(isMobile) {
					changeSrc($(this), 'mobile');
				}
			});
		};

		$(window).on('resize', function () {
			changeImg();
		});

		changeImg();
	};

	AS.wowJs = function () {
		const wow = new WOW({
			mobile: false,
		});

		wow.init();
	};

	AS.tabs = function () {
		$(document).on('click', '[data-tabclass]', function onClick(e) {
			const $this = $(this);
			const content = $this.data('tabclass');
			const number = $this.data('tabnumber');
			const isHref = $(this).attr('href');

			if(isHref) {
				e.preventDefault();
			}

			$('[data-tabclass="' + content + '"]').each(function each() {
				const $element = $(this);

				if($element.data('tabnumber') === number) {
					$element.addClass('active').siblings().removeClass('active');
				}
			});

			$('.' + content + ' > [data-tabnumber="' + number + '"]').addClass('active').siblings().removeClass('active');
		});
	};

	AS.tags = function () {
		const tags = document.querySelectorAll('.js-tags-input');

		tags.forEach(function (elem) {
			elem.addEventListener('click', function () {
				const parent = elem.closest('.js-tags-li');
				const elemList = parent.closest('.js-tags-list');
				const elemSiblings = elemList.querySelectorAll('.js-tags-li');
				const isChecked = elem.checked;

				const checked = function () {
					elemSiblings.forEach(function (elem) {
						elem.classList.remove('active');
					});
					parent.classList.add('active');
				};

				if(isChecked) {
					checked();
				}
			});
		});
	};

	// promocode events
	AS.promocodeEvents = function () {
		const $elems = $('.js-promocode');

		$elems.each(function () {
			const $parentElem = $(this);
			const $input = $parentElem.find('.promocode__input');
			const $btnReset = $parentElem.find('.js-promocode-reset');

			const isFilled = function () {
				$btnReset.addClass('active');
			};

			const isNotFilled = function () {
				$btnReset.removeClass('active');
			};

			const inputClear = function () {
				$input.val('');
			};

			inputClear();

			$input.on('keyup', function () {
				const value = $(this).val();
				const isValue = value.length >= 1;

				if(isValue) {
					isFilled();
				} else {
					isNotFilled();
				}
			});

			$btnReset.on('click', function () {
				inputClear();
				isNotFilled();
			});
		});
	};

	// specific
	AS.checkboxReset = function () {
		const $btns = $('.js-filter-reset');

		$btns.each(function () {
			$(this).on('click', function () {
				const $parentItem = $(this).closest('.js-checkbox-reset-item');
				const $inputs = $parentItem.find('.js-checkbox-input');

				$inputs.prop('checked', false);
			});
		});
	};

	AS.headerScrolled = function () {
		const elems = document.querySelectorAll('.js-page-header');

		window.addEventListener('scroll', function (e) {
			const pageY = e.pageY;
			const actionPoint = pageY >= 80;

			elems.forEach(function (elem) {
				if(actionPoint) {
					elem.classList.add('scrolled');
				} else {
					elem.classList.remove('scrolled');
				}
			});
		});
	};

	AS.scrollUp = function () {
		const $elems = $('.js-scroll-up');

		$elems.on('click', function () {
			$('html, body').animate({
				scrollTop: 0,
			}, 500);
		});
	};

	AS.scrollTo = function () {
		const $elems = $('.js-scroll-elem');

		$elems.on('click', function (e) {
			e.preventDefault;
			const scrollTarget = $(this).data('target');
			const $target = $(`.${scrollTarget}`);
			const targetOffset = $target.offset().top - 100;

			$('html, body').animate({
				scrollTop: targetOffset,
			}, 500);
		});
	};

	AS.searchEvents = function () {
		const $elems = $('.js-search-form');

		$elems.each(function () {
			const $parentElem = $(this);
			const $input = $parentElem.find('.js-search-input');
			const $btnReset = $parentElem.find('.js-search-reset');

			const isFilled = function () {
				$parentElem.addClass('filled');
			};

			const isNotFilled = function () {
				$parentElem.removeClass('filled');
			};

			const inputClear = function () {
				$input.val('');
			};

			inputClear();

			$input.on('keyup', function () {
				const value = $(this).val();
				const isValue = value.length > 0;

				if(isValue) {
					isFilled();
				} else {
					isNotFilled();
				}
			});

			$btnReset.on('click', function () {
				inputClear();
				isNotFilled();
			});
		});
	};

	AS.linkTo = function () {
		const elems = document.querySelectorAll('.js-link');

		elems.forEach(function (elem) {
			elem.addEventListener("click", function () {
				let url = this.getAttribute('data-href');

				window.location.href = url;
			});
		});
	};

	AS.rangeSlider = function () {
		const $sliders = $('.js-range-slider');
		const $resetBtns = $('.js-aside-slider-reset');
		const $valueElems = $('.js-slider-range-values');
		const $minValueElems = $('.js-slider-range-value-min');
		const $maxValueElems = $('.js-slider-range-value-max');

		// on slide event inside
		const options = {
			range: true,
			min: 0,
			max: 7000,
			values: [0, 7000],
			slide: function (event, ui) {
				const $parent = $(this).closest('.js-slider-range');
				const $minValue = $parent.find('.js-slider-range-value-min');
				const $maxValue = $parent.find('.js-slider-range-value-max');
				const $filter = $parent.find('.js-slider-range-filter');
				const positionTop = $parent.position().top;

				$minValue.val(ui.values[0]);
				$maxValue.val(ui.values[1]);
				$filter.addClass('active');
				AS.changeFilterPosition(positionTop);
			}
		};

		const hideDefaultInputArrows = function ($valueElems) {
			$valueElems.on('focus', function (e) {
				$(this).on('wheel', function (e) {
					e.preventDefault();
				});
			});
		};

		const onPriceChange = function () {
			$valueElems.on('change', function () {
				const $parent = $(this).closest('.js-slider-range');
				const positionTop = $parent.position().top;
				console.log(123);

				AS.changeFilterPosition(positionTop);
			});
		};

		const changeSliderPosition = function ($minValueElems, $maxValueElems) {
			const changePositionMin = function ($this) {
				const $parent = $this.closest('.js-aside-slider-reset-parent');
				const $slider = $parent.find('.js-range-slider');
				const value = $this.val();

				$slider.slider('values', 0, value);
			};

			const changePositionMax = function ($this) {
				const $parent = $this.closest('.js-aside-slider-reset-parent');
				const $slider = $parent.find('.js-range-slider');
				const value = $this.val();

				$slider.slider('values', 1, value);
			};

			$minValueElems.each(function () {
				$(this).on('change', function () {
					changePositionMin($(this));
				});
			});

			$maxValueElems.each(function () {
				$(this).on('change', function () {
					changePositionMax($(this));
				});
			});
		};

		const resetSlider = function ($resetBtns) {
			$resetBtns.each(function () {
				const $this = $(this);

				$this.on('click', function () {
					const $parent = $this.closest('.js-aside-slider-reset-parent');
					const $slider = $parent.find('.js-range-slider');
					const $minValueElem = $parent.find('.js-slider-range-value-min');
					const $maxValueElem = $parent.find('.js-slider-range-value-max');
					const $minValueVal = $minValueElem.attr('data-value');
					const $maxValueVal = $maxValueElem.attr('data-value');

					$slider.slider('values', 0, $minValueVal);
					$slider.slider('values', 1, $maxValueVal);

					$minValueElem.val($minValueVal);
					$maxValueElem.val($maxValueVal);
				});
			});
		};

		const initialization = function () {
			onPriceChange();
			hideDefaultInputArrows($valueElems);
			$sliders.slider(options);
			changeSliderPosition($minValueElems, $maxValueElems);
			resetSlider($resetBtns);
			$sliders.draggable();
		};

		initialization();
	};

	AS.popupEmail = function () {
		const $btnsOpen = $('.js-popup-email-open');
		const $btnsClose = $('.js-popup-email-close');
		const $popups = $('.js-popup-email');
		const $body = $('body');

		const popupShow = function ($popup) {
			$popup.addClass('active');
			$body.addClass('overflow');
		};

		const popupHide = function ($popup) {
			$popup.removeClass('active');
			$body.removeClass('overflow');
		};

		const globalPopupHide = function () {
			$popups.removeClass('active');
			$body.removeClass('overflow');
		};

		$popups.each(function () {
			const $container = $(this).find('.js-popup-email-container');

			$container.on('click', function (event) {
				event.stopPropagation();
			});
		});

		$btnsOpen.each(function () {
			$(this).on('click', function (event) {
				const popupSelector = $(this).data('popup-selector');
				const $popup = $('.' + popupSelector);
				const isActive = $popup.hasClass('active');

				event.stopPropagation();

				if(isActive) {
					popupHide($popup);
				} else {
					popupShow($popup);
				}
			});
		});

		$btnsClose.each(function () {
			$(this).on('click', function () {
				const $popup = $(this).closest('.js-popup-email');

				popupHide($popup);
			});
		});

		$(document).on('keydown.js-popup-email', function onKeyDown(evt) {
			if(evt.keyCode === AS.ESC_CODE) {
				globalPopupHide();
			}
		});
	};

	AS.popups = function () {
		const popups = document.querySelectorAll('.js-popup');
		const body = document.querySelector('body');
		const btnsOpen = document.querySelectorAll('.js-popup-btn-open');

		const popupOpen = function (btnOpen) {
			const popupData = btnOpen.getAttribute('data-popup-selector');
			const popup = document.querySelector('.' + popupData);

			body.classList.add('overflow');
			popup.classList.add('active');
		};

		const popupClose = function (btnClose) {
			const popupParent = btnClose.closest('.js-popup');

			body.classList.remove('overflow');
			popupParent.classList.remove('active');
		};

		const popupGlobalClose = function () {
			body.classList.add('overflow');
			popups.forEach(function (popup) {
				popup.classList.remove('active');
			});
		};

		popups.forEach(function (popup) {
			const btnsClose = popup.querySelectorAll('.js-popup-btn-close');

			btnsClose.forEach(function (btnClose) {
				btnClose.addEventListener('click', function () {
					popupClose(this);
				});
			});
		});

		btnsOpen.forEach(function (btnOpen) {
			btnOpen.addEventListener('click', function () {
				popupOpen(this);
			});
		});

		$(document).on('keydown.js-popup', function onKeyDown(evt) {
			if(evt.keyCode === AS.ESC_CODE) {
				popupGlobalClose();
			}
		});
	};

	AS.filterMove = function () {
		const checkboxes = document.querySelectorAll('.js-checkbox-filter');
		AS.filter = document.querySelector('.js-slider-range-filter');

		AS.changeFilterPosition = function (positionTop) {
			if(AS.lessTablet.matches) {
				return
			}

			if(AS.filter){
				AS.filter.classList.add('active');
				AS.filter.style.top = positionTop + 'px';
			}

		};

		const hideFilter = function () {
			if(AS.filter){
				AS.filter.classList.remove('active');
			}

		};

		checkboxes.forEach(function (checkbox) {
			checkbox.addEventListener('change', function () {
				const isChecked = this.checked;
				const positionTop = this.closest('.checkbox').offsetTop;

				if(isChecked) {
					AS.changeFilterPosition(positionTop);
				}
			});
		});

		window.addEventListener('resize', function () {
			hideFilter();
		});
	};

	AS.popupMsg = function (text) {
		const popup = document.querySelector('.js-popup-any-message');
		const textSelector = popup.querySelector('.js-popup-message');
		const body = document.querySelector('body');

		body.classList.add('overflow');
		popup.classList.add('active');
		textSelector.textContent = text;
	};

	// animation
	AS.childAnimation = function () {
		const elemsOnClick = document.querySelectorAll('.js-animation-parent--click');
		const elemsOnHover = document.querySelectorAll('.js-animation-parent--hover');
		const elemsOnClickTarget = document.querySelectorAll('.js-animation-target');
		const btnsClose = document.querySelectorAll('.js-animation-close');

		const animationFunctionVanilla = function (elem) {
			let parent = elem.closest('.js-animation-parent');
			const isAnimationTarget = elem.classList.contains('js-animation-target');

			if(parent === null) {
				parent = elem;
			}

			const isActive = parent.classList.contains('active');
			const animationStart = elem.getAttribute('data-animation-start');
			const animationEnd = elem.getAttribute('data-animation-end');
			let child = parent.querySelector('.js-animation-child');

			if(isAnimationTarget) {
				child = null;
			}

			if(child === null) {
				const animationSelector = elem.getAttribute('data-animation-selector');
				child = document.querySelector('.' + animationSelector);
			}

			const startAnimation = function (elem) {
				elem.classList.add('active');
				child.classList.remove(animationEnd);
				child.classList.add(animationStart);
			};

			const endAnimation = function (elem) {
				elem.classList.remove('active');
				child.classList.add(animationEnd);
				child.classList.remove(animationStart);
			};

			if(!isActive) {
				startAnimation(parent);
			} else {
				endAnimation(parent);
			}

			AS.elemsOnClickCloseOutside(parent, endAnimation);
		};

		const animationClose = function (elem) {
			const parent = elem.closest('.js-animation-parent');
			const child = parent.querySelector('.js-animation-child');
			const animationBtn = parent.querySelector('.js-animation-parent--click');
			const animationStart = animationBtn.getAttribute('data-animation-start');
			const animationEnd = animationBtn.getAttribute('data-animation-end');

			parent.classList.remove('active');
			child.classList.add(animationEnd);
			child.classList.remove(animationStart);
		};

		elemsOnClick.forEach(function (elem) {
			elem.addEventListener('click', function () {
				animationFunctionVanilla(this);
			});
		});

		elemsOnClickTarget.forEach(function (elem) {
			elem.addEventListener('click', function () {
				animationFunctionVanilla(this);
			});
		});

		btnsClose.forEach(function (elem) {
			elem.addEventListener('click', function () {
				animationClose(elem);
			});
		});

		elemsOnHover.forEach(function (elem) {
			elem.addEventListener('mouseenter', function (e) {
				const elem = e.target;

				animationFunctionVanilla(elem);
			});

			elem.addEventListener('mouseleave', function (e) {
				const elem = e.target;

				animationFunctionVanilla(elem);
			});
		});
	};

	// sliders
	AS.singleSLider = function () {
		const $slider = $('.js-single-slider');

		$slider.slick({
			dots: true,
			slidesToShow: 1,
			arrows: false,
			infinite: true,
			appendDots: '.js-single-slider-dots'
		});
	};

	AS.brandSLider = function () {
		const $slider = $('.js-brand-slider');
		const $arrPrev = $('.js-brand-arr-prev');
		const $arrNext = $('.js-brand-arr-next');

		$slider.slick({
			dots: false,
			slidesToShow: 3,
			arrows: false,
			infinite: false,
			responsive: [{
				breakpoint: 1023,
				settings: {
					slidesToShow: 2
				}
			}, ]
		});

		$arrPrev.on('click', function () {
			$slider.slick('slickPrev');
		});

		$arrNext.on('click', function () {
			$slider.slick('slickNext');
		});
	};

	AS.catalogShortSlider = function () {
		const $slider = $('.js-catalog-short-slider');
		const $window = $(window);

		function throttle(fn, frequency) {
			'use strict';

			frequency = frequency || 100;
			var timeout = false;

			return function () {
				if(timeout) {
					return;
				}

				timeout = setTimeout(function () {
					timeout = false;
				}, frequency);

				fn.apply(this, arguments);
			};
		}

		const slickOptions = {
			arrows: false,
			dots: true,
			slidesToShow: 4,
			infinite: true,
			responsive: [{
				breakpoint: 767,
				settings: {
					slidesToShow: 2
				}
			}, ]
		};

		$slider.slick(slickOptions).each(function () {
			function equalizeHeights() {
				const $this = $(this);
				const $track = $this.find('.slick-track');
				const $slides = $track.children();
				const $slidesDiv = $slides.children();
				const $slidesItem = $slidesDiv.children();
				const $slidesContent = $slidesItem.children();

				$slides.css('min-height', '');
				$slidesDiv.css('min-height', '');
				$slidesItem.css('min-height', '');
				$slidesContent.css('min-height', '');

				const minSlideHeight = $track.height();

				$slides.css('min-height', minSlideHeight);
				$slidesDiv.css('min-height', minSlideHeight);
				$slidesItem.css('min-height', minSlideHeight);
				$slidesContent.css('min-height', minSlideHeight);
			}

			var equalizeSlideHeights = throttle(equalizeHeights.bind(this), 250, true);
			$window.on('DOMContentLoaded load resize', equalizeSlideHeights);
		});
	};

	AS.catalogElemSlider = function () {
		const $slider = $('.js-catalog-elem-slider');

		const slickOptions = {
			arrows: false,
			dots: true,
			slidesToShow: 1,
			infinite: true,
			appendDots: '.catalog-elem__dots'
		};

		$slider.slick(slickOptions);
	};

	// utils
	AS.toggleClass();
	AS.accordion();
	AS.select();
	AS.isFormElemFilled();
	AS.phoneMask();
	AS.togglePassword();
	AS.bodyOverflow();
	AS.adaptiveImg();
	AS.wowJs();
	AS.tabs();
	AS.promocodeEvents();
	AS.tags();

	// specific
	AS.checkboxReset();
	AS.headerScrolled();
	AS.scrollUp();
	AS.searchEvents();
	AS.linkTo();
	AS.rangeSlider();
	AS.popupEmail();
	AS.popups();
	AS.filterMove();
	AS.scrollTo();

	// animation
	AS.childAnimation();

	// sliders
	AS.singleSLider();
	AS.brandSLider();
	AS.catalogShortSlider();
	AS.catalogElemSlider();

})();
